## Shopify Theme Development Tool

Development tool for Shopify to create themes for Online Store 2.0.

- [Shopify Theme Development Tool](#shopify-theme-development-tool)
- [Getting Started](#getting-started)
- [Commands](#commands)
- [Environment](#environment)
- [Notes](#notes)

### Features

- [Blank starter theme](https://github.com/sefryak/shopify-blank-theme)
- Webpack 5
- Switch between bespoke development and production API's that are used to extend Shopify theme capabilities
- ESLint
- Babel
- PostCSS
- Create chunks

---

## Getting Started

Please make sure the following are installed.

| Tool                                                                | Why?                                           |
| ------------------------------------------------------------------- | ---------------------------------------------- |
| [Homebrew](https://brew.sh/)                                        | To install Shopify CLI tool                    |
| [Shopify CLI](https://shopify.dev/themes/tools/cli/getting-started) | To connect to Shopify store before development |

1. Install dependencies - `npm install`
2. Create a `.env` file and include this snippet in
   ```js
   NODE_ENV = development;
   API_LIST_TARGET = development;
   ```
3. Start build and watch for any changes - `npm run start`

### Creating chunks

Webpack will create a chunk for any script or style sheet in `scripts/entries` and `styles/entries`.

| Entry                              | Output                    |
| ---------------------------------- | ------------------------- |
| `scripts/entries/collection.js`    | `assets/collection.js`    |
| `styles/entries/product-card.scss` | `assets/product-card.css` |

Webpack will output the files in `assets/` on build therefore if a new entry file is created, `npm run start` has to be ran again i'm afraid.

Example of referencing entry files in `.liquid`:

```html
<!-- USE DAWN THEME FOR BEST PRACTICES* -->
<link rel="stylesheet"  href="{{ 'product-card.css' | asset_url }}">
<script type="text/javascript" src="{{ 'collection.js' | asset_url }}" defer="defer">
```

---

## Commands

|                       | Description                                                                                                                                                                            |
| --------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `npm run build`       | Builds js/scss in development/production                                                                                                                                               |
| `npm run deploy`      | Upload your local theme files to Shopify - [Read more](https://shopify.dev/themes/tools/cli/getting-started#preview-test-and-share-your-theme)                                         |
| `npm run publish`     | Select and publish an unpublished theme from your theme library - [Read more](https://shopify.dev/themes/tools/cli/getting-started#publish-your-theme)                                 |
| `npm run start`       | Shopify CLI uploads the theme as a development theme on the store that you're connected and watches for changes - [Read more](https://shopify.dev/themes/tools/cli#development-themes) |
| `npm run test:liquid` | Lints your theme code - [Read more](https://shopify.dev/themes/tools/cli/getting-started#lint-your-themes-code)                                                                        |
| `npm run test:js`     | Check js for errors and best practices using [xo](https://github.com/xojs/xo)                                                                                                          |

---

## Environment

`NODE_ENV=(development|production)` for development or production build

`API_LIST_TARGET=(development|production)` to swap between custom development and production ready api's.

Make sure the object keys for both `development-api-list.js` and `production-api-list.js` are the same.

If only one store is used for development + testing and production, feel free to only use `production-api-list.js`.

### Usage

```js
// development-api-list.js
module.exports = {
	FOO_API: JSON.stringify('https://lambda.development-foo-api.com/bar'),
};
// production-api-list.json
module.exports = {
	FOO_API: JSON.stringify('https://lambda.production-foo-api.com/bar'),
};
```

Reference value in js files:

```js
fetch(FOO_API).then(...)
```

---

## Notes

- To minimise the number of dot files, js linting rules are defined in `package.json` with the help of [xo](https://github.com/xojs/xo) . In addition the postcss configuration is defined within `webpack/parts/common.js` rather than creating a separate configuration file for it.
- Fonts, images should be hosted in Shopify CDN, the purpose of the asset folder should be to contain all js/css chunk files.
- If i've missed out on anything please create an issue 😀.
