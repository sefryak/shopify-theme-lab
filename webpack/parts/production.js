const {DefinePlugin} = require('webpack');

module.exports = {
	mode: 'production',
	devtool: 'source-map',
	plugins: [new DefinePlugin(require(`../api/${process.env.API_LIST_TARGET}-api-list.js`))],
	stats: {children: false},
};
