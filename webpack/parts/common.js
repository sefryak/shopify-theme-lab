const glob = require('glob');
const path = require('path');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const RemoveEmptyScriptsPlugin = require('webpack-remove-empty-scripts');

module.exports = {
	entry: (() => {
		const a = glob.sync('./scripts/entries/**/*.js').reduce((acc, path) => {
			const entry = path.replace(/^.*[\\\/]/, '').replace('.js', '');
			acc[entry] = [path];
			return acc;
		}, {});

		const b = glob.sync('./styles/entries/**/*.scss').reduce((acc, path) => {
			const entry = path.replace(/^.*[\\\/]/, '').replace('.scss', '');
			if (entry in a) {
				a[entry].push(path);
			} else {
				acc[entry] = path;
			}

			return acc;
		}, {});

		return {...a, ...b};
	})(),
	output: {
		filename: './[name].js',
		path: path.resolve('./', 'assets'),
	},
	watchOptions: {
		poll: true,
		ignored: /node_modules/,
	},
	plugins: [
		new RemoveEmptyScriptsPlugin({}),
		new MiniCssExtractPlugin({
			filename: './[name].css',
		}),
	],
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				options: {
					presets: ['@babel/preset-env'],
					plugins: [],
				},
			},
			{
				test: /\.(sc|sa|c)ss$/,
				use: [
					MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader',
						options: {
							url: false,
						},
					},
					{
						loader: 'postcss-loader',
						options: {
							postcssOptions: {
								plugins: [['autoprefixer']],
							},
						},
					},
					{
						loader: 'sass-loader',
						options: {
							sourceMap: true,
						},
					},
				],
			},
		],
	},
};
