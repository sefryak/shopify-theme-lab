const {DefinePlugin} = require('webpack');
const WebpackShellPluginNext = require('webpack-shell-plugin-next');

module.exports = {
	mode: 'development',
	devtool: 'eval-cheap-source-map',
	plugins: [new DefinePlugin(require(`../api/${process.env.API_LIST_TARGET}-api-list.js`))],
	stats: 'errors-warnings',
};

if (process.argv[process.argv.length - 1] === '--watch') {
	module.exports.plugins.push(
		new WebpackShellPluginNext({
			onBuildStart: {
				scripts: ['echo build in progress'],
			},
			onBuildEnd: {
				scripts: ['echo build complete', 'shopify theme serve'],
				parallel: true,
			},
		}),
	);
}
