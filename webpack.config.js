require('dotenv').config();
const {merge} = require('webpack-merge');

module.exports = () =>
	merge(require('./webpack/parts/common'), require(`./webpack/parts/${process.env.NODE_ENV}.js`));
